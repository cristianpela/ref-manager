
var rm=require("./mod/referral");//referral module;
var dbmodule=require("./mod/database");//database module;
var data = require("sdk/self").data;
var pagemod=require("sdk/page-mod");
var timers=require("sdk/timers");
var prefs=require('sdk/simple-prefs')
var urlscan=prefs.prefs['urlscan'];
var matcher=prefs.prefs['matcher'];

var scanforchanges=false;
var query;
var existentReferrals, updatedReferrals=[];
var totalReferrals=0;

var controlpanel = require("sdk/panel").Panel({
  width: 13000,
  height: 750,
  contentScriptFile: [
        data.url("vendor/js/jquery-git2.min.js"),
        data.url("vendor/js/angular.min.js"),
        data.url("modules/controlpanel/js/managerApp.js"),
        data.url("modules/controlpanel/js/directives.js"),
        data.url("modules/controlpanel/js/services.js"),
        data.url("modules/controlpanel/js/filters.js"),
        data.url("modules/controlpanel/js/sdk.broadcasting.service.js")
    ],
  contentURL: data.url("modules/controlpanel/managerView.html")
});

controlpanel.port.on("close_control_panel",function(){
    controlpanel.hide();  
});
controlpanel.port.on("mark-recycled",function(referral){
    if(query){
        query.addReferrals([referral]).then(function(){
            console.error("Mark for recycle");
            controlpanel.port.emit("message","Referral "+referral.id+" was marked for recycling");
        });
    }
});
//##################################
var button = require('sdk/ui/button/toggle').ToggleButton({
  id: "manager-link",
  label: "Manager",
  icon: "./icon.gif",
  onClick: function onClick(){
    //handleClick();
  },
  onChange: function(state){
    if (state.checked) {
        menu.show({
          position: button
        });
    }
  }
});

//##################################
var menu = require("sdk/panel").Panel({
  width: 165,
  height: 127,
  contentScriptFile: data.url("modules/dropmenu/js/dropMenuScript.js"),
  contentURL: data.url("modules/dropmenu/dropMenu.html"),
  onHide: function() {
    button.state('window', {checked: false});
  },
});

menu.port.on("control_panel",function(){
    var scanfinished=stopScanForChanges();
    if (scanfinished) {
        scanfinished.then(function(){
            showControlPanel();
        });
    }else{
        showControlPanel();
    }
});
menu.port.on("scanforchanges",function(){
    scanforchanges=true;
    console.log("SCAN FOR >....");
    query.getReferrals().then(function(data){
       /* for(r in data){
            existentReferrals.push(data[r]);
        }*/
        existentReferrals=data;   
    });
});
menu.port.on("recycling",function(){
   // stopScanForChanges();
});
//##################################
var messagepanel= require("sdk/panel").Panel({
  width: 150,
  height: 100,
  position:{left: 10, top:10},
  contentScriptFile: data.url("modules/messages/js/messagePanelScript.js"),
  contentURL: data.url("modules/messages/messagePanel.html"),
});

function showMessage(message){
    return new Promise(function(resolve,reject){
        messagepanel.port.emit("content",message);
        messagepanel.show();
        timers.setTimeout(function(){
            messagepanel.hide();
            resolve();
        },2000);
    });
};
//##################################

dbmodule.db().open().then(function(__){
   // __.addReferral([ref]);
    query=__;
});

function showControlPanel() {
    if (query) {
        query.getReferrals().then(function(data){
            controlpanel.port.emit("data-retrieved",data);
            //panel.port.emit("data-message","Addon message");
            controlpanel.show();
        });
    }
}

function referralSort(array){
    return array.sort(function(r1,r2){
        return r1.id.localCompare(r2.id);
    });
}
function stopScanForChanges(){
    if (scanforchanges) {
        scanforchanges=false;
        if (!updatedReferrals) {
            showMessage("An error has ocurred. Scan aborted.");
        }
        if (query && updatedReferrals.length===totalReferrals) {
            var toPersistReferrals=[];
            for(r in updatedReferrals){
                var ref=updatedReferrals[r];
                if (ref.topersist) {
                    delete ref.topersist;
                    toPersistReferrals.push(ref);
                }
            }
            query.addReferrals(toPersistReferrals).then(function(){
                //deleting what remains from disjoining existentReferrals with updatedReferrals
                var toBeDeleted=existentReferrals.filter(function(existentR){
                    return !updatedReferrals.some(function(updatedR){
                        return existentR.id===updatedR.id;
                    });
                });
                if (toBeDeleted.length>0) {
                    query.deleteReferrals(toBeDeleted);
                }
                
                var promise=showMessage("Scanning complete.</br>"+
                        toPersistReferrals.length+" referrals updated/added.</br>"+
                        toBeDeleted.length+" referrals recycled/removed.");
                //query.deleteReferrals(toPersistReferrals);
                updatedReferrals=[];
                existentReferrals=[];
                return promise;
            }); 
        }else{
           return showMessage("Scan aborted. Must scan "+totalReferrals+" referrals. Currently scanned: "+updatedReferrals.length+"/"+totalReferrals);
        }
    }
    return null;
}

pagemod.PageMod({
    include: urlscan,
    contentScriptFile: data.url("modules/crawlers/scanForChanges.js"),
    onAttach: function onAttach(worker) {
        if (scanforchanges && existentReferrals) {
            worker.port.emit("scanforchanges", existentReferrals, updatedReferrals, matcher);
            worker.port.on("referrals",function(referrals,totReferrals){
                updatedReferrals=referrals;
                //totalReferrals=totReferrals;
                totalReferrals=100;
                if (updatedReferrals && updatedReferrals.length<totalReferrals) {
                    showMessage("Scanning in progress.</b> Scanned: "+ updatedReferrals.length+" out of "+totalReferrals+" referrals.");
                }else{
                    stopScanForChanges();
                    menu.port.emit("reset");
                }
            });
        }
    }
});




