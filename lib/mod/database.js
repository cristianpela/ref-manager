var { indexedDB, IDBKeyRange } = require('sdk/indexed-db');
var query;
var STORAGE="referrals";
var READ_ONLY="readonly";
var READ_WRITE="readwrite";


var DB=function(){
   this.open=function(){return open()};
   this.addReferral=function(referral){addReferral(referral)};
   this.initQuery=function(ev){initQuery(ev)};
   this.getReferrals=function(data){getReferrals(data)};
}

function open(){
   var connection=indexedDB.open("referralDB",1);
   connection.onupgradeneeded = function() {
      var result=connection.result;
      if (!result.objectStoreNames.contains(STORAGE)) {
         result.createObjectStore(STORAGE, { keyPath: "id" });
      }
   }
   var __=this;
   return new Promise(function(resolve,reject){
      connection.onerror=function(ev){
         console.log("error");
         reject(ev);
      }
      connection.onsuccess=function(ev){
         initQuery(ev);
         resolve(__);
      }
   });
   
  // return connection;
}

function initQuery(ev){
   query=ev.target.result;
}

function openTx(type){
   var tx= query.transaction([STORAGE],type);
   tx.oncomplete=function(){
      //console.log("tx complete");
   };
   tx.onabort=function(err){
      console.log(err);
   }
   return tx.objectStore(STORAGE);
}

function addReferrals(referrals){
   return new Promise(function(resolve,reject){
      var store=openTx(READ_WRITE);
      for (i=0;i< referrals.length;i++) {
         //console.error(referrals[i]);
         store.put(referrals[i]);  
      }
      resolve();
   });
}

function deleteReferrals(referrals){
   return new Promise(function(resolve,reject){
      var store=openTx(READ_WRITE);
      for (i=0;i< referrals.length;i++) {
         store.delete(referrals[i].id);
      }
      resolve();
   });
}
/*function getReferrals(data) {
   openTx(READ_ONLY).openCursor().onsuccess = function(ev) {
            var res = ev.target.result;
            if(res){
               data(res.value);
               res.continue();
            }
      };
}*/

function getReferrals() {
   return new Promise(function(resolve,reject){
      var referrals=[];
      openTx(READ_ONLY).openCursor().onsuccess = function(ev) {
            var res = ev.target.result;
            if(res){
               referrals.push(res.value);
               res.continue();
            }else{
               resolve(referrals);
            }
      };
   });
}

function getKeys(data){
   openTx(READ_ONLY).index('id').openKeyCursor().onsuccess=function(ev){
      var res = ev.target.result;
            if(res){
               data(res.value);
               res.continue();
            }
   }
}

exports.db=function(){return new DB()};