describe("Application test functionanlity",function(){
    beforeEach(module('managerApp.services'));
    describe("testing Utils - service",function(){
        var utils;
        beforeEach(inject(function(Utils){
            utils=Utils;  
        }));
        xit("should be 16 days difference",function(){
            var dif=utils.dateDifference(new Date("2015/01/01"),new Date());
            expect(dif).toBe(16);
        });
        xit("should be 16 days difference",function(){
            var dif=utils.dateDifference(new Date("2015/01/01"));
            expect(dif).toBe(16);
        });
        xit("should be 16 days difference",function(){
            var dif=utils.dateDifference("2015/01/01");
            expect(dif).toBe(16);
        });
        it("should be 16 days difference",function(){
            var dif=utils.dateDifference("2015/01/01",new Date("2015/01/17"));
            expect(dif).toBe(16);
        });
        it ('',function(){
            var nextmonth=utils.nextDate(new Date("2015/01/03"),30);
            expect(nextmonth).toBe("2015/02/02");
        });
        it ('',function(){
            var nextday=utils.nextDate(new Date("2015/01/03"),1,'%d');
            expect(nextday).toBe('04');
        });
        xit ('next 7 days',function(){
            var nextday=utils.nextDate(new Date("2015/01/03"),7,'%d');
            expect(nextday).toBe('09');
        });
    });
    describe("testing Expander - service",function(){
        var mockedReferrals=[
            {id: 'R123',
            clicklisting:[
                {date:"2015/01/03",
                clicks:4},
                {date:"2015/01/09",
                clicks:4},
                {date:"2015/01/12",
                clicks:4}
            ]},
        ];
        var expander,exapanded,details,months;
        function testday(month,week,day,toCompareDate){
            expect(months[month].weeks[week].days[day].date).toEqual(toCompareDate);
        }
        function testmonthinterval(month,starting,ending){
            var interval=months[month].interval;
            expect(interval.start).toBe(starting);
            expect(interval.end).toBe(ending);
        }
        function testweekinterval(month,week,starting,ending){
            var interval=months[month].weeks[week].interval;
            expect(interval.start).toBe(starting);
            expect(interval.end).toBe(ending);
        }
        beforeEach(inject(function(Expander){
            expander=Expander;
            expanded=expander.expand(mockedReferrals[0],new Date("2015/01/20"));
            details=expanded.details();
            months=expander.modeled(details)
        }));
        it ('',function(){
            expect(details.length).toBe(21);
        });
        it ('starting date',function(){
           testday(0,0,0,'2015/01/03');
        });
        it ('last day of the first week',function(){
           testday(0,0,6,'2015/01/09');
        });
        it ('first day of the second week',function(){
           testday(0,1,0,'2015/01/10');
        });
        it ('last day of the second week',function(){
           testday(0,1,6,'2015/01/16');
        });
        it ('test interval month',function(){
           testmonthinterval(0,"2015/01/03",'2015/01/30');
        });
        it ('test interval week 1',function(){
           testweekinterval(0,0,'03','09');
        }); 
        it ('test interval week 2',function(){
           testweekinterval(0,1,'10','16');
        });
        it ('test interval week 3',function(){
           testweekinterval(0,2,'17','23');
        });
        it ('test how many weeks are',function(){
            expect(months[0].weeks.length).toBe(3);
        });
    });
});