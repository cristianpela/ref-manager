angular.module('managerApp.services',[])
.factory('Utils',function(){
    return{
        dateDifference:function(startDate,endDate){
            endDate=(!endDate)?new Date():(endDate instanceof Date)? endDate:new Date(endDate);
            startDate=(startDate instanceof Date)? startDate:new Date(startDate);
            return Math.floor((endDate-startDate) / (1000*60*60*24));
        },
        nextDate:function(startDate,days,format){
            startDate=(startDate instanceof Date)? startDate:new Date(startDate);
            startDate.setDate(startDate.getDate()+days);
            format=format||"%Y/%m/%d";
            return this.format(startDate,format);//startDate.toLocaleFormat(format);
        },
        format:function(date,format){
            if (format==="%Y/%m/%d") {
                var m=date.getMonth()+1;
                var y=date.getFullYear();
                var d=date.getDate();
                return y+'/'+((m<10)?('0'+m):m)+"/"+((d<10)?('0'+d):d);
            }else{
                var d=date.getDate();
                return ((d<10)?('0'+d):""+d);
            }
        }
    }
})
.factory('Expander',function(Utils){
    var expand=function(referral, tailDate){
        var list=referral.clicklisting.slice();
        var today=tailDate||new Date();
        if (list[list.length-1].date!==Utils.format(today,"%Y/%m/%d")) {
            list.push({
                //"date":today.toLocaleFormat("%Y/%m/%d"),
                "date":Utils.format(today,"%Y/%m/%d"),
                "clicks":0
            });
        }
        this.details=function(){
            var details=[];
            for(var l=0;l<list.length;l++){
                var clickdata=list[l];
                var nextclickdata=list[l+1];
                details.push(clickdata);
                if (nextclickdata) {
                    var curdate=new Date(clickdata.date);
                    var nextdate=new Date(nextclickdata.date);
                    var dif=Utils.dateDifference(curdate,nextdate);
                    if (dif>1) {
                        for(var i=1;i<dif;i++){
                            curdate.setDate(curdate.getDate()+1)
                            details.push({
                                date: Utils.format(curdate,"%Y/%m/%d"),
                                clicks:0
                            });
                        }
                    }
                }
            }
            var len=details.length;
            if (len%7!==0) {
                while(len%7!==0){
                    today.setDate(today.getDate()+1);
                    details.push({
                                date: Utils.format(today,"%Y/%m/%d"),
                                clicks:0
                            });
                    len++;
                }
            }          
            return details;
        };
    };
    var modeled=function(expanded){
            var months=[],monthcount=0,daycount=0,weekcount=0, lastweekinmonth=false;
            months.push(
                {clicks:0,
                interval:{start:expanded[0].date,end:Utils.nextDate(expanded[0].date,27)},
                weeks:[]});
            for(var ex=0;ex<expanded.length;ex++){
                if (daycount===0 || daycount===-2) {
                    var startdate=expanded[ex].date;
                    var justday=startdate.substring(startdate.lastIndexOf('/')+1);
                    months[monthcount].weeks.push(
                                                {clicks:0,
                                                interval:{start: justday, end: Utils.nextDate(expanded[ex].date,6,'%d')},
                                                days:[]});
                }
                months[monthcount].clicks+=+expanded[ex].clicks;
                months[monthcount].weeks[weekcount].clicks+=+expanded[ex].clicks;
                months[monthcount].weeks[weekcount].days.push(expanded[ex]);
                if (daycount++===6) {
                    weekcount++;
                    daycount=0;
                }
                if (weekcount===4 && ex!==expanded.length-1) {
                    monthcount++;
                    weekcount=0;
                    var startint=Utils.nextDate(expanded[ex].date,1);
                    months.push(
                       {clicks:0,
                        interval:{start:startint,end:Utils.nextDate(startint,27)},
                        weeks:[]});
                }
            }
            return months;
        };
    return {
            expand: function(referral, tailDate){
                return new expand(referral, tailDate);
            },
            modeled:function(expanded){
                return modeled(expanded);  
            }
    }
});