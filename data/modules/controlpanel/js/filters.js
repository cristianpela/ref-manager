angular.module('managerApp.filters',[])
.filter("filteredReferrals",function(){
    
    var recycledReferrals=function(referrals){
        recyrefs=[];
        angular.forEach(referrals,function(r){
            if(r.toberecycled)
                recyrefs.push(r);
        });
        return recyrefs;
    }
    
    return function(referrals,type){
        if(type==='recy')
            return recycledReferrals(referrals);
        return referrals;
    }
})