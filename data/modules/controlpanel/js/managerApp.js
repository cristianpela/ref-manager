var app=angular.module('managerApp',
   ['sdk.broadcasting.service',
    'managerApp.services',
    'managerApp.directives',
    'managerApp.filters']);
app.controller("dataListCtrl",function($scope, Expander, Utils, SDKBroadcast){
    $scope.referrals=[];
    $scope.filterType="";
    $scope.detailed=function(referral){
        $scope.currentreferral=referral;
        $scope.expander=Expander.expand(referral).details();
        $scope.daysSinceLastClick=Utils.dateDifference(referral.lastclick);
    };
    $scope.recycle=function(referral){
        referral.toberecycled=true;
        SDKBroadcast.emit("mark-recycled",referral);
    };
    $scope.close=function(){
        SDKBroadcast.emit("close_control_panel");
    };
    SDKBroadcast.on("data-retrieved",function(data){
        $scope.referrals=data;
        console.error(data);
    });
    SDKBroadcast.on("message",function(data){
        $scope.message=data;
    });
});

