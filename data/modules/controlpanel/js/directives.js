angular.module('managerApp.directives',[])
.directive("content",function(Expander,Utils){
    return{
        restrict:'E',
        scope:{
                data:"=data"
            },
        templateUrl:"partials/referralChart.html",
        controller: function crtl($scope){
            var today=new Date();
            $scope.showTooltip=function(entry){
                $scope.tooltip=entry.date;
            };
            $scope.hasPassedToday=function(date){
                return Utils.dateDifference(today,date)>=0;
            };
        },
        link:function(scope,element,attrs){
            scope.$watch('data',function(){
                scope.model=Expander.modeled(scope.data);
                element.hide(function(){
                    element.slideToggle();
                });
            });
        }
    }
});