function emitcommand(cmd) {
    self.port.emit("cmd");
}
self.port.on("reset",function(){
    var radios=document.querySelectorAll('input[type]');
    for(var radio=0, len=radios.length;radio<len;radio++){
      var r=radios[radio];
      if(r.checked){
        r.checked=!r.checked;
      }
    }
});
window.addEventListener("click",function(event){
    var target=event.target;
    self.port.emit(target.id);
    if (target.id==="control_panel") {
        target.checked=false;
    }
});