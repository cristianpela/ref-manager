
self.port.on("scanforchanges",function(existentReferrals, updatedReferrals, matcher){
    var url=window.location.href;
    if (url.indexOf(matcher)>0) {
        var idcount=1;
        while(true){
            var refNode=document.getElementById("fgidu_"+idcount);
            if (refNode) {
                var referral={};
                referral.id=refNode.firstChild.nodeValue.trim();
                var sibcount=0;
                var sib=refNode.nextSibling;
                while(sibcount<5){// only need the next 5 siblings down the branch
                    var data=sib.firstChild.nodeValue.trim();
                    switch (sibcount) {
                        case 0: referral.since=textDateToMili(data);break;
                        case 1: referral.next=textDateToMili(data);break;
                        case 2: referral.lastclick=(data.indexOf("yet")>-1)?referral.since:textDateToMili(data);break;
                        case 3: referral.clicks=data;break;
                        case 4: referral.avg=data;break;
                    }
                    sibcount++;
                    sib=sib.nextSibling;
                }
                if(!containsInUpdated(updatedReferrals,referral)){
                    var r=needToUpdate(existentReferrals,referral);
                    updatedReferrals.push(r);
                };
                idcount++;
            }else{
                break;
            }
        }
        var totalReferrals=+document.querySelector("span.f_b").innerHTML.trim();
        self.port.emit("referrals",updatedReferrals,totalReferrals);
    }
});

function containsInUpdated(updatedReferrals, currentReferral) {
    for(r in updatedReferrals){
        if (updatedReferrals[r].id===currentReferral.id) {
            return true;
        }
    }
    return false;
}

function needToUpdate(existentReferrals, currentReferral){
    var newref=true, updatedref=false;
    for(r in existentReferrals){
        var ref=existentReferrals[r];
        if (ref.id===currentReferral.id) {
            newref=false;
            if(ref.clicks!==currentReferral.clicks){
                updatedref=true;
                var newclick=currentReferral.clicks-ref.clicks;
                ref.lastclick=currentReferral.lastclick;
                ref.clicks=currentReferral.clicks;
                ref.avg=currentReferral.avg;
                currentReferral=ref;
                var lastclicklisting=currentReferral.clicklisting[currentReferral.clicklisting.length-1];
                if (currentReferral.lastclick===lastclicklisting.date) {
                    lastclicklisting.clicks=+newclick;
                    currentReferral.clicklisting[currentReferral.clicklisting.length-1].clicks=lastclicklisting.clicks;
                }else{
                    currentReferral.clicklisting.push({
                        date: currentReferral.lastclick,
                        clicks: newclick
                    });
                }
                currentReferral.topersist=true;
            }
            break;
        }
    }
    if (newref) {
        currentReferral.clicklisting=[];
        currentReferral.clicklisting.push({
            date: currentReferral.lastclick,
            clicks: currentReferral.clicks
        });
        currentReferral.topersist=true;
        currentReferral.toberecycled=false;
    }
    return currentReferral;
}

function textDateToMili(txtdate) {
    if (txtdate.indexOf("Today")>-1) {
        return new Date().toLocaleFormat('%Y/%m/%d');
    }else if (txtdate.indexOf("Yesterday")>-1) {
        return backToThePast(1).toLocaleFormat('%Y/%m/%d');
    }else if (txtdate.indexOf("days")>-1) {
        txtdate=+txtdate.substring(0, txtdate.indexOf(" "));
        return backToThePast(1).toLocaleFormat('%Y/%m/%d');
    }else if (txtdate.indexOf("at")>-1) {
        txtdate=txtdate.substring(0, txtdate.indexOf(" "));
        return new Date(txtdate).toLocaleFormat('%Y/%m/%d');
    }
    return null;
}

function backToThePast(days){
    var date=new Date();
    date.setDate(date.getDate()-days);
    return date;
}